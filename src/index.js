import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';

// Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));

// App component is our mother component, this is the component we use as entry point and where we can render all other components or pages.
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
