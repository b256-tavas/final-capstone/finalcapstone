import Banner from '../components/Banner';
import Login from '../pages/Login'

export default function Home() {
    const data = {
        title: "The Gadget Store",
        content: "Gadget for everyone",
        destination: "/products",
        label: "Buy now"
    }

    return (
        <>
            <Banner data={data}/>
    

        </>
    )
}
