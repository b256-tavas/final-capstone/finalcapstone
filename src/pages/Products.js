import { useState, useEffect, useContext } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import ProductCard from "../components/ProductCard";
import UserContext from '../userContext';
import React from "react";
import Swal from 'sweetalert2';

export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  const [showActiveProducts, setShowActiveProducts] = useState(false);
  const [searchProductId, setSearchProductId] = useState("");

  const [show, setShow] = useState(false);
  const [disabledButton, setDisabledFormButton] = useState(true);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const initialFormData = Object.freeze({
    name: "",
    description: "",
    price: 1,
  });
  const [formData, updateFormData] = useState(initialFormData);

  const handleChange = (e) => {
    updateFormData({
      ...formData,
      // Trimming any whitespace
      [e.target.name]: e.target.value.trim(),
    });

    if (
      formData.name !== "" &&
      formData.description !== "" &&
      formData.price !== 0
    ) {
      setDisabledFormButton(false);
    }
  };

const handleSearch = () => {
  if (searchProductId.trim() !== "") {
    fetch(`http://localhost:4000/products/${searchProductId}`)
      .then((res) => res.json())
      .then((data) => {
        // Update the products state with the searched product or handle the response data as needed
        setProducts([data]);
      })
      .catch((error) => {
        console.error("Error searching for product:", error);
      });
  }
};


  const fetchActiveProducts = () => {
    fetch("http://localhost:4000/products/activeProducts")
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.error("Error fetching active products:", error);
      });
  };

  useEffect(() => {
    if (showActiveProducts) {
      fetchActiveProducts();
    } else {
      fetchProductList(user);
    }
  }, [showActiveProducts, user]);


  const fetchProductList = (user) => {
    const url = user.isAdmin ? "http://localhost:4000/products/allProducts" : "http://localhost:4000/products/activeProducts";
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.error("Error fetching products:", error);
      });
  };

  const handleToggleActiveProducts = () => {
    setShowActiveProducts((prevShowActiveProducts) => !prevShowActiveProducts);
  };

  const handleAddProduct = () => {
    handleShow();
  };

  const submitForm = () => {
    handleClose();
    fetch("http://localhost:4000/products/create", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(formData),
    })
      .then((response) => response.json())
      .then((data) => {
        Swal.fire({
          title: "Product Added",
          icon: "success",
          text: "Product successfully added."
        });
        if (showActiveProducts) {
          fetchActiveProducts();
        } else {
          fetchProductList(user);
        }
      })
      .catch((error) => {
        console.error("Error adding product:", error);
      });
  };

  const handleUpdateList = () => {
    if (showActiveProducts) {
      fetchActiveProducts();
    } else {
      fetchProductList(user);
    }
  };

  return (
    <div>
      <div className="d-flex justify-content-between mb-2">
        <h3>Products</h3>
        {user.isAdmin ? (
          <Button variant="primary" onClick={handleAddProduct}>
            Add Product
          </Button>
        ) : (
          ""
        )}
      </div>

      {user.isAdmin && (
  <>
    {showActiveProducts ? (
      <Button variant="secondary" onClick={handleToggleActiveProducts}>
        All Products
      </Button>
    ) : (
      <Button variant="primary" onClick={handleToggleActiveProducts}>
        Active Products
      </Button>
    )}
  </>
)}


{user.isAdmin && (
  <Form.Group className="mb-3">
    <Form.Label>Search by Product ID</Form.Label>
    <Form.Control
      type="text"
      name="searchProductId"
      required
      value={searchProductId}
      onChange={(e) => setSearchProductId(e.target.value)}
    />
  </Form.Group>
)}
{user.isAdmin && (
  <Button variant="primary" onClick={handleSearch}>
    Search
  </Button>
)}




      {products.map((product) => (
        <ProductCard
          key={product._id}
          product={product}
          onUpdateProduct={handleUpdateList}
        />
      ))}

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                name="name"
                autoFocus
                required
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                name="description"
                required
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                name="price"
                required
                onChange={handleChange}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            onClick={submitForm}
            disabled={disabledButton}
          >
            Add
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
