import './App.css';
import { useState, useEffect } from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { UserProvider} from './userContext';
import {Container} from 'react-bootstrap';
import NavBar from './components/NavBar'
import Register from './pages/Register';
import Login from './pages/Login'
import Logout from './pages/Logout'
import Home from './pages/Home'
import Products from './pages/Products'
import AdminDashboard from './pages/AdminDashboard'
import Productview from './pages/Productview'



function App() {

 const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });
  
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect (() => {
    if (localStorage) {
      setUser({
        id: localStorage.id,
        isAdmin: localStorage.isAdmin
      });
    }
  },[]);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <NavBar />
      <Container>
    <Routes>
      <Route path='/' element={<Home />} />
          <Route path='/register' element={<Register/>} />
          <Route path='/login' element={<Login/>} />
          <Route path='/logout' element={<Logout/>} />
          <Route path='/products' element={<Products/>} />
          <Route path='/AdminDashboard' element={<AdminDashboard />} />
          <Route path="/Productview/:productId" element={<Productview />}/>



        </Routes>
      </Container>  
    </Router>
    </UserProvider>
  );

}

export default App;

