import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import { useState, useEffect, useContext } from "react";
import React from "react";
import Swal from "sweetalert2";
import UserContext from "../userContext";

export default function ProductCard(props) {
  const { name, description, price, _id, isActive } = props.product;
  const { user } = useContext(UserContext);
  const handleUpdateClose = () => setUpdateShow(false);
  const handleUpdateShow = () => setUpdateShow(true);
  const [updateShow, setUpdateShow] = useState(false);
  const handleRemoveReactivateClose = () => setRemoveReactivateShow(false);
  const handleRemoveReactivateShow = () => setRemoveReactivateShow(true);
  const [removeReactivateShow, setRemoveReactivateShow] = useState(false);
  const [triggerProductText, setTriggerProduct] = useState("");
  const [disabledButton, setDisabledFormButton] = useState(false);
  const initialFormData = Object.freeze({
    name: "",
    description: "",
    price: 1,
    _id: "",
  });
  const [formData, updateFormData] = React.useState(initialFormData);

  const handleUpdateProduct = () => {
    handleUpdateShow();
  };

  const handleRemoveProduct = () => {
    setTriggerProduct("Remove");
    handleRemoveReactivateShow();
  };

  const handleReactivateProduct = () => {
    setTriggerProduct("Reactivate");
    handleRemoveReactivateShow();
  };

  const handleChange = (e) => {
    updateFormData({
      ...formData,

      // Trimming any whitespace
      [e.target.name]: e.target.value,
    });

    if (
      formData.name !== "" &&
      formData.description !== "" &&
      formData.price !== 0
    ) {
      setDisabledFormButton(false);
    }
  };

  const removeReactivateProduct = () => {
    handleRemoveReactivateClose();
    const method = triggerProductText === "Remove" ? "archive" : "activate";
    fetch(`http://localhost:4000/products/${method}/${formData._id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        if (response && response === true) {
          Swal.fire({
            title: `${triggerProductText} Product`,
            icon: "success",
            text: `Product successfully updated.`,
          });
          props.onUpdateProduct();
        }
      })
      .catch((error) => {
        console.error(
          `Error ${triggerProductText.toLocaleLowerCase()} product:`,
          error
        );
      });
  };

  const submitForm = () => {
    handleUpdateClose();

    const payload = {
      name: formData.name,
      description: formData.description,
      price: formData.price,
    };
    fetch(`http://localhost:4000/products/update/${formData._id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(payload),
    })
      .then((response) => response.json())
      .then((response) => {
        if (response && response === true) {
          Swal.fire({
            title: "Product Updated",
            icon: "success",
            text: "Product successfully updated.",
          });
          props.onUpdateProduct();
        }
      })
      .catch((error) => {
        console.error("Error updating product:", error);
      });
  };

  useEffect(() => {
    updateFormData(props.product);
  }, [props.product]);

  return (
    <div>
      <Card className="mt-1">
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>{price}</Card.Text>

          {user.isAdmin ? (
            <Button
              variant="primary"
              className="me-2"
              onClick={handleUpdateProduct}
            >
              Edit
            </Button>
          ) : (
            ""
          )}

          {/* Check if user is Admin - if user is admin display button to remove or reactivate product */}
          {user.isAdmin ? (
            <Button
              variant="primary"
              className="me-2"
              onClick={isActive ? handleRemoveProduct : handleReactivateProduct}
            >
              {isActive ? "Remove" : "Reactivate"}
            </Button>
          ) : (
            ""
          )}

          <Button variant="primary" as={Link} to={`/Productview/${_id}`}>
            Details
          </Button>
        </Card.Body>
      </Card>

      {/* Update product modal */}
      <Modal show={updateShow} onHide={handleUpdateClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            <Form>
              <Form.Group className="mb-3">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  value={formData.name}
                  autoFocus
                  required
                  onChange={handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  name="description"
                  value={formData.description}
                  required
                  onChange={handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  name="price"
                  value={formData.price}
                  required
                  onChange={handleChange}
                />
              </Form.Group>
            </Form>
          }
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            onClick={submitForm}
            disabled={disabledButton}
          >
            Update
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Remove or Reactivate modal */}
      <Modal show={removeReactivateShow} onHide={handleRemoveReactivateClose}>
        <Modal.Header closeButton>
          <Modal.Title>{triggerProductText} Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {"Are you sure to " +
            triggerProductText.toLowerCase() +
            " the product?"}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleRemoveReactivateClose}>
            Close
          </Button>
          <Button variant="primary" onClick={removeReactivateProduct}>
            {triggerProductText}
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

