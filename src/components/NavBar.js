import { useContext } from 'react';
import { Container,Navbar,Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../userContext'

export default function NavBar() {

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">The Gadget Store</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
            {
              (user.id !== null)?
                <Nav.Link as={NavLink} to="/logout">Log Out</Nav.Link>
                :
                <>
                <Nav.Link as={NavLink} to="/login">LogIn</Nav.Link>

                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                </>

            }
    
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

  )
}
